<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dialog extends Model
{
    protected $table = 'dialog';

    protected $fillable = [
        'sender_id',
        'receiver_id',
        'message',
        'ride_id',
        'date_time'
    ];

    public function sender()
    {
        return $this->belongsTo('App\User', 'sender_id', 'id');
    }

    public function receiver()
    {
        return $this->belongsTo('App\User', 'receiver_id', 'id');
    }
}
