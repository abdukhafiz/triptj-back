<?php

namespace App\Http\Controllers;

use App\Repositories\Dialog\DialogRepositoryInterface;
use Illuminate\Http\Request;

class DialogController extends Controller
{

    protected $dialog;

    public function __construct(DialogRepositoryInterface $dialog)
    {
        $this->dialog = $dialog;
    }

    /**
     * Get driver passenger dialog
     * @param Request $request
     * @return mixed
     */
    public function getDialog(Request $request)
    {
        return $this->dialog->get($request);
    }

    /**
     * store message and send push notification
     * @param Request $request
     * @return mixed
     */
    public function storeMessage(Request $request)
    {
        return $this->dialog->store($request);
    }

}
