<?php

namespace App\Http\Controllers;

use App\Repositories\User\UserRepositoryInterface;
use App\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    protected $user;

    /**
     * Create a new controller instance.
     *
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->user = $userRepository;
    }

    /**
     * Show drivers whose waiting for confirmation
     *
     * @return Renderable
     */
    public function index()
    {
        $users = $this->user->getUsersForIdentification();
        return view('home', compact('users'));
    }

    public function privacy()
    {
        return view('privacy');
    }
}
