<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ImageController extends Controller
{

    /**
     * Resize image on the fly
     * @param Request $request
     * @param $imageName
     * @param int $imageType
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function resize(Request $request, $imageName, $imageType = 1)      // imageType: 1 - avatar, 2 - car, 3 - driver license, 4 - passport
    {
        if(!is_null($imageName) && $imageName != '')
        {
            $width = null;
            $height = null;

            if(!is_null($request->get('width')))
                $width = $request->get('width');
            if(!is_null($request->get('height')))
                $height = $request->get('height');

            switch ($imageType) {
                case 1:
                    $imgLocation = 'img/avatar/'.$imageName;
                    break;
                case 2:
                    $imgLocation = 'img/car/'.$imageName;
                    break;
                case 3:
                    $imgLocation = 'img/driver_license/'.$imageName;
                    break;
                case 4:
                    $imgLocation = 'img/passport/'.$imageName;
                    break;

                default:
                    $imgLocation = 'img/'.$imageName;
            }

            if(!file_exists($imgLocation))
                response()->json('No image');

            $img = Image::make($imgLocation)->fit($width, $height);

            return $img->response('png');
        }

        return response()->json('No image');
    }

}
