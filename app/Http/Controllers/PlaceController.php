<?php

namespace App\Http\Controllers;

use App\Repositories\Place\PlaceRepositoryInterface;
use Illuminate\Http\Request;

class PlaceController extends Controller
{

    protected $place;

    public function __construct(PlaceRepositoryInterface $place)
    {
        $this->place = $place;
    }

    /**
     * List of all places.
     * @return mixed
     */
    public function index()
    {
        return $this->place->all();
    }

}
