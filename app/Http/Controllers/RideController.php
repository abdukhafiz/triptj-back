<?php

namespace App\Http\Controllers;

use App\Repositories\Ride\RideRepositoryInterface;
use Illuminate\Http\Request;

class RideController extends Controller
{

    protected $ride;

    public function __construct(RideRepositoryInterface $rideRepository)
    {
        $this->ride = $rideRepository;
    }


    /**
     * accept pending ride
     * @param Request $request
     * @return mixed
     */
    public function acceptRide(Request $request)
    {
        return $this->ride->acceptRide($request);
    }

    /**
     * list of my rides
     * @param Request $request
     * @return mixed
     */
    public function getRides(Request $request)
    {
        return $this->ride->getRides($request);
    }

    /**
     * get ride by id
     * @param Request $request
     * @param $rideId
     * @return mixed
     */
    public function get(Request $request, $rideId)
    {
        return $this->ride->get($request, $rideId);
    }

    /**
     * rate ride
     * @param Request $request
     * @return mixed
     */
    public function rateRide(Request $request)
    {
        return $this->ride->rateRide($request);
    }

    /**
     * Change status of ride
     * @param Request $request
     * @return mixed
     */
    public function changeStatus(Request $request)
    {
        return $this->ride->changeStatus($request);
    }

    /**
     * Offer ride (store)
     * @param Request $request
     * @return mixed
     */
    public function offerRide(Request $request)
    {
        return $this->ride->offerRide($request);
    }

    /**
     * Accept (from passenger) new ride price
     * @param Request $request
     * @return mixed
     */
    public function acceptNewPrice(Request $request)
    {
        return $this->ride->acceptNewPrice($request);
    }

}
