<?php

namespace App\Http\Controllers;

use App\Sms;
use Illuminate\Http\Request;

class SmsController extends Controller
{

    private const SHOULD_RETRY = 2;

    public function sendSms($phone, $myMessage, $type)
    {

        if(!is_null($phone) && !is_null($myMessage) && strlen($myMessage) > 0 && !is_null($type))
        {
            $sms = new Sms();
            $sms->phone = $phone;
            $sms->message = $myMessage;
            $sms->sms_type = $type;         // 0 - sign up; 1 - driver identification; 2 - reset password;
            $sms->save();

            if(!is_null($sms->id))
            {
                $dlm = ";";
                $phone_number = $phone; //номер телефона
                $txn_id = 'safari'.$sms->id; //ID сообщения в вашей базе данных, оно должно быть уникальным для каждого сообщения
//                $txn_id = $sms->id; //ID сообщения в вашей базе данных, оно должно быть уникальным для каждого сообщения
                $str_hash = hash('sha256',$txn_id.$dlm.config('custom.sms.login').$dlm.config('custom.sms.sender').$dlm.$phone_number.$dlm.config('custom.sms.hash'));
                $message = $myMessage;
                $params = array(
                    "from" => config('custom.sms.sender'),
                    "phone_number" => $phone_number,
                    "msg" => $message,
                    "str_hash" => $str_hash,
                    "txn_id" => $txn_id,
                    "t" => 23,
                    "login" => config('custom.sms.login'),
                );
                $result = $this->call_api(config('custom.sms.server'), "GET", $params);
                if ((isset($result['error']) && $result['error'] == 0)){
                    $response = json_decode($result['msg']);

                    if(!is_null($response))
                    {
                        /* так выглядет ответ сервера
                        * {
                               "status": "ok",
                               "timestamp": "2017-07-07 16:58:12",
                               "txn_id": "f890b43b964c2801f62b61a9662efff96dbaa82e007bc60c22ec41d9b22a3e0b",
                               "msg_id": 40127,
                               "smsc_msg_id": "45f22479",
                               "smsc_msg_status": "success",
                               "smsc_msg_parts": 1
                           }
                        */

                        $sms->msg_id = $response->msg_id;
                        $sms->server_response = json_encode($response);
                        $sms->save();

                        return true;
//                        $result['success']['statusCode'] = config("constants.statusCode.success");
//                        return response()->json($result['success']);
                    }
                }

                return false;
//                $result['error']['statusCode'] = config("constants.statusCode.smsNotSent");
//                $result['error']['errorMessage'] = "Невозможно отправить СМС. Повторите еще раз";
//                return response()->json($result['error']);
            }

            return null;

        }
        else
        {
            return null;
        }
    }

    /**
     * Standart function of SMS gateway
     * @param $url
     * @param $method
     * @param $params
     * @return array
     */
    private function call_api($url, $method, $params)
    {
        $curl = curl_init();
        $data = http_build_query ($params);
        if ($method == "GET") {
            curl_setopt ($curl, CURLOPT_URL, "$url?$data");
        }else if($method == "POST"){
            curl_setopt ($curl, CURLOPT_URL, $url);
            curl_setopt ($curl, CURLOPT_POSTFIELDS, $data);
        }else if($method == "PUT"){
            curl_setopt ($curl, CURLOPT_URL, $url);
            curl_setopt ($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded','Content-Length:'.strlen($data)));
            curl_setopt ($curl, CURLOPT_POSTFIELDS, $data);
        }else if ($method == "DELETE"){
            curl_setopt ($curl, CURLOPT_URL, "$url?$data");
            curl_setopt ($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        }else{
            dd("unkonwn method");
        }
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        $arr = array();
        if ($err) {
            $arr['error'] = 1;
            $arr['msg'] = $err;
        } else {
            $res = json_decode($response);
            if (isset($res->error)){
                $arr['error'] = 1;
                $arr['msg'] = "Error Code: ". $res->error->code . " Message: " . $res->error->msg;
            }else{
                $arr['error'] = 0;
                $arr['msg'] = $response;
            }
        }
        return $arr;
    }


    public function testSendSms($phone, $myMessage) {
        if(!is_null($phone) && !is_null($myMessage) && strlen($myMessage) > 0)
        {
            $dlm = ";";
            $phone_number = $phone; //номер телефона
            $txn_id = 'safaritest'.$myMessage; //ID сообщения в вашей базе данных, оно должно быть уникальным для каждого сообщения
//                $txn_id = $sms->id; //ID сообщения в вашей базе данных, оно должно быть уникальным для каждого сообщения
            $str_hash = hash('sha256',$txn_id.$dlm.config('custom.sms.login').$dlm.config('custom.sms.sender').$dlm.$phone_number.$dlm.config('custom.sms.hash'));
            $message = $myMessage;
            $params = array(
                "from" => config('custom.sms.sender'),
                "phone_number" => $phone_number,
                "msg" => $message,
                "str_hash" => $str_hash,
                "txn_id" => $txn_id,
                "t" => 23,
                "login" => config('custom.sms.login'),
            );
            $result = $this->call_api(config('custom.sms.server'), "GET", $params);
            if ((isset($result['error']) && $result['error'] == 0)){
                $response = json_decode($result['msg']);

                if(!is_null($response))
                {
                    /* так выглядет ответ сервера
                    * {
                           "status": "ok",
                           "timestamp": "2017-07-07 16:58:12",
                           "txn_id": "f890b43b964c2801f62b61a9662efff96dbaa82e007bc60c22ec41d9b22a3e0b",
                           "msg_id": 40127,
                           "smsc_msg_id": "45f22479",
                           "smsc_msg_status": "success",
                           "smsc_msg_parts": 1
                       }
                    */

                    return "fisondam";
                }
            }
            else {
                dd($result);
            }

            return false;
        }
    }

}
