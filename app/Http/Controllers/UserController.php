<?php

namespace App\Http\Controllers;

use App\Repositories\FirebaseToken\FirebaseTokenRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Http\Request;

class UserController extends Controller
{

    protected $user;
    protected $firebaseToken;

    public function __construct(
        UserRepositoryInterface $userRepository,
        FirebaseTokenRepositoryInterface $firebaseTokenRepository
    )
    {
        $this->user = $userRepository;
        $this->firebaseToken = $firebaseTokenRepository;
    }

    /**
     * Get user detail
     * @param Request $request
     * @return mixed
     */
    public function detail(Request $request)
    {
        return $this->user->userDetail($request);
    }

    /**
     * Driver identification
     * @param Request $request
     * @return mixed
     */
    public function identifyDriver(Request $request)
    {
        return $this->user->identifyDriver($request);
    }

    /**
     * Store firebase token
     * @param Request $request
     * @return mixed
     */
    public function storeFirebaseToken(Request $request)
    {
        return $this->firebaseToken->store($request);
    }

    /**
     * Get firebase token
     * @param Request $request
     * @return mixed
     */
    public function getFirebaseToken(Request $request)
    {
        return $this->firebaseToken->get($request);
    }

    /**
     * User authentication
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        return $this->user->login($request);
    }


    /**
     * User logout
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        return $this->user->logout();
    }

    /**
     * switch driver passenger
     * @param Request $request
     * @return mixed
     */
    public function switchDriverPassenger(Request $request)
    {
        return $this->user->switchDriverPassenger($request);
    }

    /**
     * update user profile
     * @param Request $request
     * @return mixed
     */
    public function updateProfile(Request $request)
    {
        return $this->user->updateProfile($request);
    }

    /**
     * upload passport, driver license and car's photos
     * @param Request $request
     * @return mixed
     */
    public function driverUploadPhotos(Request $request)
    {
        return $this->user->driverUploadPhotos($request);
    }

    /**
     * Register new User from API
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        return $this->user->register($request);
    }


    /**
     * verify phone number after registration
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyPhone(Request $request)
    {
        return $this->user->verifyPhone($request);
    }


    /**
     * reset password
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetPassword(Request $request)
    {
        return $this->resetPassword($request);
    }


    /**
     * send verification code when reseting password
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function resetPasswordGetCode(Request $request)
    {
        return $this->user->resetPasswordGetCode($request);
    }

    /**
     * Get list of users
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function usersList(Request $request)
    {
        $users = $this->user->usersList($request);

//        return  $users->firstItem();
        return view('user.list', compact('users'));
    }


}
