<?php

namespace App\Http\Middleware;

use function App\Utils\customHash;
use Closure;

class CheckHash
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $myHash = $request->get("phone")."_".$request->get("name")."+".$request->get("password")."=";

        if($request->header('hash') != customHash($myHash))
        {
            $result['error']['errorMessage'] = 'Wrong hash';
            $result['error']['statusCode'] = config('constants.statusCode.wrongHash');
            return response()->json($result['error']);
        }

        return $next($request);
    }
}
