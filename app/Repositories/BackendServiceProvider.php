<?php
/**
 * Created by PhpStorm.
 * User: helloworld
 * Date: 8/18/19
 * Time: 17:25
 */

namespace App\Repositories;
use Illuminate\Support\ServiceProvider;

class BackendServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Place\PlaceRepositoryInterface',
            'App\Repositories\Place\PlaceRepository'
        );

        $this->app->bind(
            'App\Repositories\User\UserRepositoryInterface',
            'App\Repositories\User\UserRepository'
        );

        $this->app->bind(
            'App\Repositories\Ride\RideRepositoryInterface',
            'App\Repositories\Ride\RideRepository'
        );

        $this->app->bind(
            'App\Repositories\FirebaseToken\FirebaseTokenRepositoryInterface',
            'App\Repositories\FirebaseToken\FirebaseTokenRepository'
        );

        $this->app->bind(
            'App\Repositories\Dialog\DialogRepositoryInterface',
            'App\Repositories\Dialog\DialogRepository'
        );
    }
}