<?php

namespace App\Repositories\Dialog;

use App\Dialog;
use App\FirebaseToken;
use App\Ride;
use function App\Utils\curlFirebasePushNotification;
use function App\Utils\falseValidation;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class DialogRepository implements DialogRepositoryInterface
{

    /**
     * get chat message
     * @param $request
     * @return mixed
     */
    public function get($request)
    {
        $validator = Validator::make($request->all(), [
            'ride_id' => 'required',
//            'start_from' => 'required'
        ]);

//        $start_from = (!is_null($request->get('start_from'))) ? $request->get('start_from') : 0;
//        $limit = 30;

        if ($validator->fails())
            return falseValidation($validator);

        $dialog = Dialog::select('id', 'sender_id', 'receiver_id', 'message', 'ride_id', 'date_time')
            ->with([
                'sender' => function($query) {
                    $query->addSelect('id', 'name', 'image');
                },
                'receiver' => function($query) {
                    $query->addSelect('id', 'name');
                }
            ])
            ->where('ride_id', $request->get('ride_id'))
//            ->offset($request->get('start_from'))
//            ->limit($limit)
            ->orderBy('id', 'desc')
            ->get();

        $result['statusCode'] = config("constants.statusCode.success");
        $result['dialogs'] = $dialog;

        return response()->json($result);
    }

    /**
     * store message
     * @param $request
     * @return mixed
     */
    public function store($request)
    {
        $validator = Validator::make($request->all(), [
            'receiver_id' => 'required',
            'message' => 'required',
            'ride_id' => 'required'
        ]);

        if ($validator->fails())
            return falseValidation($validator);

        $dialog = new Dialog();
//        $dialog->sender_id = 2;
        $dialog->sender_id = $request->user()->id;
        $dialog->receiver_id = $request->get('receiver_id');
        $dialog->message = $request->get('message');
        $dialog->ride_id = $request->get('ride_id');
        $dialog->date_time = Carbon::now();
        $dialog->save();

        if($dialog->id != null)
        {
            $ride = Ride::with([
                'destinationFrom',
                'destinationTo'
            ])
                ->where('id', $dialog->ride_id)
                ->first();

            $receiverToken = FirebaseToken::select('token')
                ->where('user_id', $dialog->receiver_id)
                ->first();

            $fcmMsg = [
                'title' => 'Чат водителя с пассажиром',
                'body' => $ride->destinationFrom->name . ' - ' . $ride->destinationTo->name .' ('. Carbon::parse($ride->ride_date_time)->format('d.m.Y') .')',
                'sound' => 'default'
            ];

            $fcmFields = array(
                'to' => $receiverToken->token,
                'notification' => $fcmMsg,

                // you can send additional fields with the code below
                'data' => [
                    'type' => 'driver_passenger_chat',
                    'dialog' => $dialog
                ]
            );

            curlFirebasePushNotification($fcmFields);

            $result['success']['statusCode'] = config("constants.statusCode.success");
            return response()->json($result['success']);
        }
        else
        {
            $result['error']['statusCode'] = config("constants.statusCode.otherError");
            $result['error']['errorMessage'] = "Невозможно сохранить сообщение. Пожалуйста отправьте еще раз.";

            return response()->json($result['error']);
        }

    }

}
