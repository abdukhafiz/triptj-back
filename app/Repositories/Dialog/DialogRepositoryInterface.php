<?php

namespace App\Repositories\Dialog;

interface DialogRepositoryInterface
{

    /**
     * get chat message
     * @param $request
     * @return mixed
     */
    public function get($request);

    /**
     * store message
     * @param $request
     * @return mixed
     */
    public function store($request);

}