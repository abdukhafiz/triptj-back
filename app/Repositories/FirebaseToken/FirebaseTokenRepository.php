<?php
/**
 * Created by PhpStorm.
 * User: helloworld
 * Date: 9/28/19
 * Time: 20:56
 */

namespace App\Repositories\FirebaseToken;

use App\FirebaseToken;
use function App\Utils\falseValidation;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class FirebaseTokenRepository implements FirebaseTokenRepositoryInterface
{

    /**
     * get token of specific user
     * @param $request
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function get($request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        if ($validator->fails())
            return falseValidation($validator);

        $token = FirebaseToken::where('user_id', $request->get('user_id'))->first();
        if(!$token)
        {
            $result['success']['statusCode'] = config("constants.statusCode.success");
            $result['success']['token'] = $token;

            return $result['success'];
        }

        $result['error']['statusCode'] = config("constants.statusCode.notFound");
        $result['error']['errorMessage'] = "Токен для пользователя не существует";

        return response()->json($result['error']);
    }

    /**
     * Insert firebase token
     * @param $request
     * @return mixed
     */
    public function store($request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
        ]);

        if ($validator->fails())
            return falseValidation($validator);

        FirebaseToken::updateOrCreate(
            [
                'user_id' => $request->user()->id
            ],
            [
                'token' => $request->get('token')
            ]
        );
        $result['success']['statusCode'] = config("constants.statusCode.success");

        return response()->json($result['success']);
    }

}
