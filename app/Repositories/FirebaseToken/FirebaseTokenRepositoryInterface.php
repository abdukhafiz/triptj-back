<?php
/**
 * Created by PhpStorm.
 * User: helloworld
 * Date: 9/28/19
 * Time: 20:57
 */

namespace App\Repositories\FirebaseToken;

interface FirebaseTokenRepositoryInterface
{

    /**
     * get token of user
     * @param $request
     * @return mixed
     */
    public function get($request);

    /**
     * Insert firebase token
     * @param $request
     * @return mixed
     */
    public function store($request);

}