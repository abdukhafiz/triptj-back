<?php
/**
 * Created by PhpStorm.
 * User: helloworld
 * Date: 8/18/19
 * Time: 17:20
 */

namespace App\Repositories\Place;

use App\Place;

class PlaceRepository implements PlaceRepositoryInterface
{

    /**
     * Get place by it's ID
     *
     * @param $placeId
     * @return mixed
     */
    public function get($placeId)
    {
        return Place::find($placeId);
    }

    /**
     * Get all cities
     *
     * @return mixed
     */
    public function all()
    {
        return Place::orderBy('name')->get();
    }

    /**
     * Store new place
     * @param $name
     * @param null $sort
     * @return mixed
     */
    public function store($name, $sort = null)
    {
        $place = new Place();
        $place->name = $name;
        $place->sort = $sort;
        $place->save();

        return $place;
    }
}
