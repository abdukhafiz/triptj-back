<?php
/**
 * Created by PhpStorm.
 * User: helloworld
 * Date: 8/18/19
 * Time: 17:17
 */

namespace App\Repositories\Place;

interface PlaceRepositoryInterface
{

    /**
     * Get place by it's ID
     *
     * @param $placeId
     * @return mixed
     */
    public function get($placeId);

    /**
     * Get all places
     *
     * @return mixed
     */
    public function all();

    /**
     * Store new place
     * @param $name
     * @param null $sort
     * @return mixed
     */
    public function store($name, $sort = null);

}
