<?php
/**
 * Created by PhpStorm.
 * User: helloworld
 * Date: 9/4/19
 * Time: 21:55
 */

namespace App\Repositories\Ride;

use App\FirebaseToken;
use App\Repositories\Place\PlaceRepository;
use App\Repositories\User\UserRepository;
use App\Ride;
use App\RidePrice;
use App\RideRating;
use App\User;
use Illuminate\Support\Facades\Log;
use function App\Utils\curlFirebasePushNotification;
use function App\Utils\falseValidation;
use function App\Utils\rideStatusLog;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class RideRepository implements RideRepositoryInterface
{

    /**
     * accept ride
     * @param $request
     * @return mixed
     */
    public function acceptRide($request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required'         // ride ID
        ]);

        if ($validator->fails())
            return falseValidation($validator);

        if($request->user()->type == 1)     // driver
        {
            // check driver identification only for drivers
            $userRepository = new UserRepository();
            if($userRepository->shouldDriverIdentified() == 1)
            {
                $result['error']['statusCode'] = config("constants.statusCode.notIdentified");
                $result['error']['errorMessage'] = "Пройдите идентификацию";
                return response()->json($result['error']);
            }

            $rideWhereCondition = ['passenger_id', '<>', $request->user()->id];
        }
        else
        {          // passenger
            $rideWhereCondition = ['driver_id', '<>', $request->user()->id];
        }

        $ride = Ride::with([
            'destinationFrom',
            'destinationTo'
        ])
            ->where(
                [
                    ['id', '=', $request->get('id')],
                    ['status', '=', 0],
                    $rideWhereCondition
                ]
            )->first();

        if(!$ride)
        {
            $params['status']['='] = 0;

            if($request->user()->type == 1)
                $params['passenger_id']['<>'] = $request->user()->id;
            else
                $params['driver_id']['<>'] = $request->user()->id;

            $result['error']['rides'] = $this->fetchRides($params);     // get updated list of rides
            $result['error']['statusCode'] = config("constants.statusCode.notFound");
            $result['error']['errorMessage'] = "Поездка уже получена другим водителем";
            return response()->json($result['error']);
        }

        // driver has offered new price
        $oldPrice = $ride->price;
        if($ride->price != $request->get('price'))
        {
            if($request->user()->type == 1){
                $ride->driver_id = $request->user()->id;
                $title = "Водитель предложил новую цену";
            }
            else {
                $ride->passenger_id = $request->user()->id;
                $title = "Пассажир предложил новую цену";
            }
            $ride->status = 5;

            $this->storeRidePrice($request->get('id'), $request->get('price'));     // store new price for ride
            $result['success']['hasNewPrice'] = $request->get('price');
        }
        else
        {
            // ride is confirmed
            $ride->status = 3;
            $title = "Поездка подтверждена";
        }

        if($request->user()->type == 1)
            $ride->driver_id = $request->user()->id;
        else
            $ride->passenger_id = $request->user()->id;
        $ride->save();

        // get user firebase token
        $firebaseToken = FirebaseToken::select('token')
            ->where('user_id', $ride->passenger_id)
            ->first();

        $fcmMsg = [
            'title' => $title,
            'body' => $ride->destinationFrom->name . ' - ' . $ride->destinationTo->name .' ('. Carbon::parse($ride->ride_date_time)->format('d.m.Y') .')',
            'sound' => 'default'
        ];

        $fcmFields = array(
            'to' => $firebaseToken->token,
            'notification' => $fcmMsg,

            // you can send additional fields with the code below
            'data' => [
                'type' => 'change_status',
                'ride_id' => $ride->id,
//                'type' => 'new_price_for_passenger',
//                'notification_dialog' => $title,
                'ride' => [
//                    'ride_id' => $ride->id,
                    'new_price' => $request->get('price'),
                    'old_price' => $oldPrice,
                    'status' => $ride->status
                ]
            ]
        );

        curlFirebasePushNotification($fcmFields);

        // log ride status
        rideStatusLog($ride->id, $request->user()->id, $ride->status);

        $result['success']['rides'][] = $ride;
        $result['success']['statusCode'] = config("constants.statusCode.success");
        return response()->json($result['success']);
    }

    /**
     * list of my rides
     * @param $request
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function getRides($request)
    {
        $userId = $request->user()->id;
        $userType = $request->user()->type;

        if(!is_null($request->get('status')))
            $params['status']['='] = $request->get('status');      // 0 - pending (searching driver), 1 - success (rides finished), 2 - cancelled, 3 - confirmed

        if($userId != null)
        {
            if($request->get('status') > -1) {              // if status is null
                if($userType == 0) {
                    $params['driver_id']['<>'] = $userId;
                }
                elseif($userType == 1) {
                    $params['passenger_id']['<>'] = $userId;
                }
            }
            else {
                if($userType == 1) {
                    $params['driver_id']['='] = $userId;
                }
                elseif($userType == 0) {
                    $params['passenger_id']['='] = $userId;
                }
            }

//            if($userType == 0 && $request->get('status') == 0)      // for pending rides, which driver is not equal to current passenger
//                $params['driver_id']['<>'] = $userId;
//            elseif($userType == 1 && $request->get('status') == 0)      // for pending rides, which passenger is not equal to current driver
//                $params['passenger_id']['<>'] = $userId;
//            elseif($userType == 0)
//                $params['passenger_id']['='] = $userId;
//            elseif($userType == 1 && is_null($request->get('status')))
//                $params['driver_id']['='] = $userId;

            $rides = $this->fetchRides($params);

            $result['success']['rides'] = $rides;
            $result['success']['statusCode'] = config("constants.statusCode.success");

            return response()->json($result['success']);
        }

        $result['error']['statusCode'] = config("constants.statusCode.notFound");
        $result['error']['errorMessage'] = config("Не найдено. Попробуйте заново.");
        return response()->json($result['error']);
    }

    /**
     * get rides
     * @param $params
     * @return mixed
     */
    private function fetchRides($params) {
        $query = Ride::select('id', 'driver_id', 'passenger_id', 'from_place_id', 'to_place_id', 'ride_date_time', 'price', 'status', 'type', 'comment')
            ->with(
                array(
                    'driver' => function($query) {
                        $query->addSelect('id', 'phone', 'name', 'image', 'type', 'rating');
                    },
                    'passenger' => function($query) {
                        $query->addSelect('id', 'phone', 'name', 'image', 'type');
                    },
                    'destinationFrom' => function($query) {
                        $query->addSelect('id', 'name');
                    },
                    'destinationTo' => function($query) {
                        $query->addSelect('id', 'name');
                    },
                    'prices' => function($query) {
                        $query->addSelect('id', 'ride_id', 'price');
                    }
                )
            );

        if(isset($params['passenger_id']) && !is_null($params['passenger_id'])){
            if(array_key_first($params['passenger_id']) == '<>')
            {
                $query->where(function ($query) use ($params) {
                    $query->where('passenger_id', '<>', $params['passenger_id'])
                        ->whereNull('driver_id');
                });
            }
            else
            {
                $query->where('passenger_id', array_key_first($params['passenger_id']), $params['passenger_id']);
            }
        }

        if(isset($params['driver_id']) && !is_null($params['driver_id'])){
            if(array_key_first($params['driver_id']) == '<>')
            {
                $query->where(function ($query) use ($params){
                    $query->where('driver_id', '<>', $params['driver_id'])
                        ->whereNull('passenger_id');
                });
            }
            else
            {
                $query->where('driver_id', array_key_first($params['driver_id']), $params['driver_id']);
            }
        }

        if(isset($params['status']) && !is_null($params['status']))
            $query->where('status', array_key_first($params['status']), $params['status']);

        $rides = $query->orderBy('ride_date_time', 'desc')
            ->get();

        return $rides;
    }

    /**
     * get ride for selected id
     * @param $request
     * @param $rideId
     * @return mixed
     */
    public function get($request, $rideId)
    {
        if($rideId != null)
        {
            $ride = Ride::where('id', '=', $rideId)
                ->with(
                    array(
                        'driver' => function($query) {
                            $query->addSelect('id', 'phone', 'name', 'image', 'type', 'rating');
                        },
                        'passenger' => function($query) {
                            $query->addSelect('id', 'phone', 'name', 'image', 'type');
                        },
                        'destinationFrom' => function($query) {
                            $query->addSelect('id', 'name');
                        },
                        'destinationTo' => function($query) {
                            $query->addSelect('id', 'name');
                        },
                        'prices' => function($query) {
                            $query->addSelect('id', 'ride_id', 'price');
                        }
                    )
                )
                ->get();

            $result['success']['rides'] = $ride;
            $result['success']['statusCode'] = config("constants.statusCode.success");

            return response()->json($result['success']);
        }

        $result['error']['statusCode'] = config("constants.statusCode.notFound");
        $result['error']['errorMessage'] = config("Не найдено. Попробуйте заново.");
        return response()->json($result['error']);
    }

    /**
     * rate driver
     * @param $request
     * @return mixed
     */
    public function rateRide($request)
    {
        $validator = Validator::make($request->all(), [
            'passenger_id' => 'required',
            'driver_id' => 'required',
            'ride_id' => 'required',
        ]);

        if($validator->fails())
            return falseValidation($validator);

        $rideRating = RideRating::updateOrCreate(
            [
                'ride_id' => $request->get('ride_id'),
                'passenger_id' => $request->get('passenger_id'),
                'driver_id' => $request->get('driver_id')
            ],
            [   'rating' => $request->get('rating'),
                'feedback' => $request->get('feedback')
            ]
        );

        $result['success']['rateRide'] = $rideRating;
        $result['success']['statusCode'] = config("constants.statusCode.success");

        return response()->json($result['success']);
    }

    /**
     * offer ride
     * @param $request
     * @return mixed
     */
    public function offerRide($request)
    {
        $validator = Validator::make($request->all(), [
            'price' => 'required'
        ]);

        if ($validator->fails())
            return falseValidation($validator);

        $from_place_id = $request->get('from_place_id');
        $to_place_id = $request->get('to_place_id');

        $placeRepo = new PlaceRepository();
        if($request->get('from_place_id') == -1) {
            $fromPlace = $request->get('destination_from');
            $fromPlaceObj = $placeRepo->store($fromPlace['name']);
        }
        if($request->get('to_place_id') == -1) {
            $toPlace = $request->get('destination_to');
            $toPlaceObj = $placeRepo->store($toPlace['name']);
        }

        $ride = new Ride();
        if(!is_null($request->get('passenger_id')))
            $ride->passenger_id = $request->get('passenger_id');

        if(!is_null($request->get('driver_id')))
            $ride->driver_id = $request->get('driver_id');

        if(isset($fromPlaceObj) && !is_null($fromPlaceObj))
            $ride->from_place_id = $fromPlaceObj->id;
        else
            $ride->from_place_id = $from_place_id;

        if(isset($toPlaceObj) && !is_null($toPlaceObj))
            $ride->to_place_id = $toPlaceObj->id;
        else
            $ride->to_place_id = $to_place_id;

        $ride->ride_date_time = Carbon::parse($request->get('ride_date_time'))->format("Y-m-d H:i");
        $ride->price = $request->get('price');
        $ride->status = 0;
        $ride->type = $request->get('type');
        $ride->comment = $request->get('comment');
        $ride->save();

        $sRide = $this->getRideById($ride->id);

        $tokens = User::select('id')
            ->with(
                [
                    'firebaseToken' => function($query) {
                        $query->addSelect('user_id', 'token');
                    }
                ]
            )
            ->where([
                ['type', 1],
                ['id', '<>', $request->user()->id]
            ])
            ->get()
            ->pluck('firebaseToken.token');

        $title = 'Новая поездка';
        $fcmMsg = [
            'title' => $title,
            'body' => $sRide->destinationFrom->name . ' - ' . $sRide->destinationTo->name .' ('. $sRide->price .' смн)',
            'sound' => 'default'
        ];

        $fcmFields = array(
            'registration_ids' => $tokens,
            'notification' => $fcmMsg,

            // you can send additional fields with the code below
            'data' => [
                'type' => 'store_ride'
            ]
        );

        curlFirebasePushNotification($fcmFields);

        // log ride status
        rideStatusLog($ride->id, $request->user()->id, $ride->status);

        $result['success']['statusCode'] = config("constants.statusCode.success");

        return response()->json($result['success']);
    }

    /**
     * change ride type
     * @param $request
     * @return mixed
     */
    public function changeStatus($request)
    {
        // ride status'
        // 0 - pending (searching driver), 1 - success (rides finished), 2 - cancelled, 3 - confirmed, 4 - ride started, 5 - waiting for price confirmation

        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails())
            return falseValidation($validator);

        $ride = Ride::with([
            'destinationFrom',
            'destinationTo'
        ])
            ->where('id', $request->get('id'))->first();

        $ride->status = $request->get('status');
        $ride->save();

        if($ride->status != 0) {
            $currentUserId = $request->user()->id;
            // get user firebase token
            if($ride->driver_id == $currentUserId) {
                $firebaseToken = FirebaseToken::select('token')
                    ->where('user_id', $ride->passenger_id)
                    ->first();
            }
            else {
                $firebaseToken = FirebaseToken::select('token')
                    ->where('user_id', $ride->driver_id)
                    ->first();
            }

            $title = "";
            switch ($ride->status) {
                case 1:
                    $title = "Поездка закончена";
                    break;
                case 2:
                    $title = "Поездка отменена";
                    break;
                case 3:
                    $title = "Поездка подтверждена";
                    break;
                case 4:
                    $title = "Поездка началась";
                    break;
            }

            $fcmMsg = [
                'title' => $title,
                'body' => $ride->destinationFrom->name . ' - ' . $ride->destinationTo->name .' ('. Carbon::parse($ride->ride_date_time)->format('d.m.Y') .')',
                'sound' => 'default'
            ];

            $fcmFields = array(
                'to' => $firebaseToken->token,
                'notification' => $fcmMsg,

                // you can send additional fields with the code below
                'data' => [
                    'type' => 'change_status',
                    'ride_id' => $ride->id,
                    'notification_dialog' => $title,
                    'ride' => [
                        'status' => $ride->status
                    ]
                ]
            );

            curlFirebasePushNotification($fcmFields);
        }

        // log ride status
        rideStatusLog($ride->id, $request->user()->id, $ride->status);

        $result['success']['rides'][] = $ride;
        $result['success']['statusCode'] = config("constants.statusCode.success");
        return response()->json($result['success']);

    }

    /**
     * Store ride price
     * @param $rideId
     * @param $price
     * @return mixed
     */
    private function storeRidePrice($rideId, $price)
    {
        $ridePrice = new RidePrice();
        $ridePrice->ride_id = $rideId;
        $ridePrice->price = $price;
        $ridePrice->save();

        return $ridePrice;
    }

    /**
     * Accept ride with new price
     * @param $request
     * @return mixed
     */
    public function acceptNewPrice($request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required'         // ride ID
        ]);

        if ($validator->fails())
            return falseValidation($validator);

        $ride = Ride::with([
            'destinationFrom',
            'destinationTo'
        ])
            ->where(
                array(
                    ['id', '=', $request->get('id')],
                    ['status', '=', 5]
                )
            )->first();

        if($ride)
        {
            $latestPrice = RidePrice::select('ride_id', 'price')
                ->where('ride_id', $ride->id)
                ->orderBy('id', 'desc')
                ->first();      // get latest row

            $ride->price = $latestPrice->price;
            $ride->status = 3;      // ride is confirmed
            $ride->save();

            if($request->user()->type == 1)
                $userId = $ride->driver_id;
            else
                $userId = $ride->passenger_id;

            $firebaseToken = FirebaseToken::select('token')
                ->where('user_id', $userId)
                ->first();

            $title = 'Поездка с новой ценой подтверждена';
            $fcmMsg = [
                'title' => $title,
                'body' => $ride->destinationFrom->name . ' - ' . $ride->destinationTo->name .' ('. $ride->price .' смн)',
                'sound' => 'default'
            ];

            $fcmFields = array(
                'to' => $firebaseToken->token,
                'notification' => $fcmMsg,

                // you can send additional fields with the code below
                'data' => [
                    'type' => 'change_status',
                    'ride_id' => $ride->id,
                    'notification_dialog' => $title,
//                    'type' => 'ride_accepted_with_new_price',
                    'ride' => [
                        'ride_id' => $ride->id
                    ]
                ]
            );

            curlFirebasePushNotification($fcmFields);

            // log ride status
            rideStatusLog($ride->id, $request->user()->id, $ride->status);

            $result['success']['rides'][] = $ride;
            $result['success']['statusCode'] = config("constants.statusCode.success");
            return response()->json($result['success']);
        }

        $result['error']['statusCode'] = config("constants.statusCode.notFound");
        $result['error']['errorMessage'] = "Поездка уже получена другим водителем";
        return response()->json($result['error']);
    }

    /**
     * Get ride by it's ID
     * @param $rideId
     * @return mixed
     */
    private function getRideById($rideId)
    {
        if (!is_null($rideId) && $rideId > 0)
        {
            return Ride::where('id', '=', $rideId)
                ->with(
                    array(
                        'driver' => function($query) {
                            $query->addSelect('id', 'phone', 'name', 'image', 'type', 'rating');
                        },
                        'passenger' => function($query) {
                            $query->addSelect('id', 'phone', 'name', 'image', 'type');
                        },
                        'destinationFrom' => function($query) {
                            $query->addSelect('id', 'name');
                        },
                        'destinationTo' => function($query) {
                            $query->addSelect('id', 'name');
                        },
                        'prices' => function($query) {
                            $query->addSelect('id', 'ride_id', 'price');
                        }
                    )
                )
                ->first();
        }

        return null;
    }
}
