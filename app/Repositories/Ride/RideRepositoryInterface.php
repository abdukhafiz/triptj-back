<?php
/**
 * Created by PhpStorm.
 * User: helloworld
 * Date: 9/4/19
 * Time: 21:55
 */

namespace App\Repositories\Ride;

interface RideRepositoryInterface
{

    /**
     * accept ride
     * @param $request
     * @return mixed
     */
    public function acceptRide($request);

    /**
     * list of my rides
     * @param $request
     * @return mixed
     */
    public function getRides($request);

    /**
     * get ride for selected id
     * @param $request
     * @param $rideId
     * @return mixed
     */
    public function get($request, $rideId);

    /**
     * rate driver
     * @param $request
     * @return mixed
     */
    public function rateRide($request);

    /**
     * cancel ride
     * @param $request
     * @return mixed
     */
    public function changeStatus($request);

    /**
     * offer ride
     * @param $request
     * @return mixed
     */
    public function offerRide($request);

    /**
     * Accept ride with new price
     * @param $request
     * @return mixed
     */
    public function acceptNewPrice($request);

}
