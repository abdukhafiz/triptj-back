<?php
/**
 * Created by PhpStorm.
 * User: helloworld
 * Date: 9/4/19
 * Time: 11:21
 */

namespace App\Repositories\User;

use App\Http\Controllers\SmsController;
use App\User;
use function App\Utils\base64ToImage;
use function App\Utils\falseValidation;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class UserRepository implements UserRepositoryInterface
{

    /**
     * Get user detail
     * @param $request
     * @return mixed
     */
    public function userDetail($request)
    {
        $user = Auth::user();
        $user['should_identify_driver'] = $this->shouldDriverIdentified();      // 1 - driver should be identified, 2 - not identified yet (has some days left to identifying), 0 - already identified
        return $user;
    }

    /**
     * Confirm driver identification
     * @param $request
     * @return mixed
     */
    public function identifyDriver($request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'identified' => 'required'
        ]);

        if($validator->fails())
        {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = User::where('id', $request->get('user_id'))
            ->first();
        if($user)
        {
            $user->identified = $request->get('identified');
            $user->update();

            // TODO: log driver identification to DB

            if($request->get('identified') == 1)
                $smsText = 'Ваш профиль идентифицирован.';
            else
                $smsText = 'Ваш профиль не идентифицирован.';

            $sms = new SmsController();
            $sms->sendSms($user->phone, $smsText, 1);

            return back()->with('success', 'Статус водителя изменен');
        }

        return back()->with('error', 'Ошибка. Обновите страницу и повторите еще раз');

    }

    /**
     * Get list of users for identification
     * @return mixed
     */
    public function getUsersForIdentification()
    {
        return User::select('id', 'phone', 'name', 'cargo', 'birth_date', 'photo_passport', 'photo_driver_license', 'photo_car')
            ->where([
                ['admin', 0],
                ['identified', 2]     // status drivers waiting for identification
            ])
            ->orderBy('id', 'desc')
            ->get();
    }

    /**
     * User authentication
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login($request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|min:9|max:9',
            'password' => 'required|min:6|max:25'
        ]);

        if ($validator->fails())
            return falseValidation($validator);

        if(Auth::attempt(['phone' => $request->get('phone'), 'password' => $request->get('password')]))
        {
            $user = Auth::user();

            $result['success']['user'] = $user;
            $result['success']['user']['token'] = $user->createToken(config("custom.tokenString"))->accessToken;
            $result['success']['user']['should_identify_driver'] = $this->shouldDriverIdentified();      // 1 - driver should be identified, 2 - not identified yet (has some days left to identifying), 0 - already identified

            $result['success']['statusCode'] = config("constants.statusCode.success");

            return response()->json($result['success']);
        }
        else
        {
            $result['error']['errorMessage'] = 'Неправильный номер телефона или пароль';
            $result['error']['statusCode'] = config('constants.statusCode.unauthorized');
            return response()->json($result['error']);
        }
    }


    /**
     * User logout
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $accessToken = Auth::user()->token();
        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);

        $accessToken->revoke();
        if(isset($accessToken)){
            $result['success']['statusCode'] = config("constants.statusCode.loggedOut");
            return response()->json($result['success']);
        }
    }


    /**
     * switch driver passenger
     * @param $request
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function switchDriverPassenger($request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required'
        ]);

        if ($validator->fails())
            return falseValidation($validator);

        $user = User::findOrFail($request->user()->id);
        $user->type = $request->get('type');
        $user->update();

        $result['success']['user'] = $user;
        $result['success']['statusCode'] = config("constants.statusCode.success");
        return response()->json($result['success']);
    }


    /**
     * update user profile
     * @param $request
     * @return mixed
     */
    public function updateProfile($request)
    {
        $validator = Validator::make($request->all(), [
            'birth_date' => 'required',
            'password' => 'nullable|min:6|max:25'
        ]);

        if ($validator->fails())
            return falseValidation($validator);

        // update user profile
        $user = User::where('id', $request->user()->id)
            ->first();
        if($user)
        {
            $user->name = $request->get('name');
            $user->birth_date = Carbon::parse($request->get('birth_date'))->format("Y-m-d");
            if($request->get('email') != null)
                $user->email = $request->get('email');
            if($request->get('password') != null && strlen($request->get('password')) > 5)
                $user->password = bcrypt($request->get("password"));

            if(!is_null($request->get('image'))){
                $filePath = 'img/avatar/';

                if(!is_dir($filePath)) mkdir($filePath);

                $fileName = md5(time() . config("constants.salt")) . ".png";
                base64ToImage($request->get('image'), $filePath.$fileName);

                if(strlen($user->image) > 0 && file_exists($filePath . $user->image))
                    unlink($filePath . $user->image);

                $user->image = $fileName;
            }
            $user->update();

            if(!is_null($request->get('password')) && strlen($request->get('password')) > 5)
            {
                $result['success']['user'] = $user;
                $result['success']['user']['token'] = $user->createToken(config("custom.tokenString"))->accessToken;

                $result['success']['statusCode'] = config("constants.statusCode.success");
                return response()->json($result['success']);
            }

            $result['success']['user'] = $user;
            $result['success']['user']['token'] = null;
            $result['success']['statusCode'] = config("constants.statusCode.success");
            return response()->json($result['success']);
        }
        else
        {
            $result['error']['statusCode'] = config("constants.statusCode.notFound");
            $result['error']['errorMessage'] = "Не действительный токен";
            return response()->json($result['error']);
        }
    }


    /**
     * driver upload scan of passport, driver licence and car license
     * @param $request
     * @return mixed
     */
    public function driverUploadPhotos($request)
    {
        $validator = Validator::make($request->all(), [
            'photo_passport' => 'required',
            'photo_driver_license' => 'required',
            'photo_car' => 'required'
        ]);

        if ($validator->fails())
            return falseValidation($validator);

        $user = User::where('id', $request->user()->id)
            ->first();

        if($user)
        {
            if(!is_null($request->get('cargo'))) $user->cargo = $request->get('cargo');
            if(!is_null($request->get('identified'))) $user->identified = $request->get('identified');

            if(!is_null($request->get('photo_passport')))
            {
                $passportDir = 'img/passport/';
                if(!is_dir($passportDir)) mkdir($passportDir);

                $passportName = md5(time() . 'passport' . config('constants.salt')) . '.png';
                base64ToImage($request->get('photo_passport'), $passportDir.$passportName);

                if(strlen($user->photo_passport) > 0 && file_exists($passportDir . $user->photo_passport))
                    unlink($passportDir . $user->photo_passport);

                $user->photo_passport = $passportName;
            }

            if(!is_null($request->get('photo_driver_license')))
            {
                $licenseDir = 'img/driver_license/';
                if(!is_dir($licenseDir)) mkdir($licenseDir);

                $licenseName = md5(time() . 'license' . config('constants.salt')) . '.png';
                base64ToImage($request->get('photo_driver_license'), $licenseDir.$licenseName);

                if(strlen($user->photo_driver_license) > 0 && file_exists($licenseDir . $user->photo_driver_license))
                    unlink($licenseDir . $user->photo_driver_license);

                $user->photo_driver_license = $licenseName;
            }

            if(!is_null($request->get('photo_car')))
            {
                $carPhotoDir = 'img/car/';
                if(!is_dir($carPhotoDir)) mkdir($carPhotoDir);

                $carPhotoName = md5(time() . 'carphoto' . config('constants.salt')) . '.png';
                base64ToImage($request->get('photo_car'), $carPhotoDir.$carPhotoName);

                if(strlen($user->photo_car) > 0 && file_exists($carPhotoDir . $user->photo_car))
                    unlink($carPhotoDir . $user->photo_car);

                $user->photo_car = $carPhotoName;
            }
            $user->update();

            $result['success']['user'] = $user;
            $result['success']['statusCode'] = config("constants.statusCode.success");
            return response()->json($result['success']);
        }
    }


    /**
     * Register new User from API
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register($request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|min:9|max:9|unique:users',
            'name' => 'required|max:100',
            'password' => 'required|min:6|max:25'
        ]);

        if($validator->fails())
            return falseValidation($validator);

        $verificationCode = $this->generateVerificationCode();

        $user = new User();
        $user->name = $request->get("name");
        $user->phone = $request->get("phone");
        $user->password = bcrypt($request->get("password"));
        $user->verification_code = $verificationCode;
        $user->type = 0;        // passenger

        $smsText = 'Код активации - '. $verificationCode;
        $sms = new SmsController();
        $smsResult = $sms->sendSms($user->phone, $smsText, 0);

        if(!is_null($smsResult) && $smsResult)
        {
            $user->save();

            $result['success']['name'] = $user['name'];
            $result['success']['phone'] = $user['phone'];
            $result['success']['verification_code'] = $user['verification_code'];
            $result['success']['statusCode'] = config("constants.statusCode.success");

            return response()->json($result['success']);
        }
        else
        {
            $result['error']['statusCode'] = config("constants.statusCode.smsNotSent");
            $result['error']['errorMessage'] = "Невозможно отправить СМС. Повторите еще раз";
            return response()->json($result['error']);
        }

    }


    /**
     * verify phone number after registration
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyPhone($request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|min:9|max:9',
            'verification_code' => 'required|min:6|max:6',
        ]);

        if($validator->fails())
            return falseValidation($validator);

        $user = User::select('id', 'phone', 'name', 'verification_code')
            ->where([
                ['phone', '=', $request->get('phone')],
                ['verification_code', '=', $request->get('verification_code')]
            ])
            ->whereNull('phone_verified_at')
            ->first();

        // if number is not exist
        $result['error']['errorMessage'] = 'Неправильный код или номер не существует';
        $result['error']['statusCode'] = config('constants.statusCode.incorrectConfirmationCode');

        if(is_null($user))
            return response()->json($result['error']);


        if($user->verification_code == $request->get('verification_code'))
        {
            $user->phone_verified_at = Carbon::now()->toDateTimeString();
            $user->update();

            $result['success']['user'] = $user;
            $result['success']['statusCode'] = config("constants.statusCode.success");

            return response()->json($result['success']);
        }

        return response()->json($result['error']);

    }


    /**
     * reset password
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetPassword($request)
    {

        $validator = Validator::make($request->all(), [
            'phone' => 'required|min:9|max:9',
            'verification_code' => 'required',
            'password' => 'required'
        ]);

        if($validator->fails())
            return falseValidation($validator);

        $user = User::where([
            ['phone', '=', $request->get('phone')],
            ['verification_code', '=', $request->get('verification_code')]
        ])->first();

        // if number is not exist
        $result['error']['errorMessage'] = 'Неправильный код или номер не существует';
        $result['error']['statusCode'] = config('constants.statusCode.incorrectConfirmationCode');

        if($user->verification_code == $request->get('verification_code'))
        {
            $user->password = bcrypt($request->get("password"));
            $user->save();

            $result['success']['statusCode'] = config("constants.statusCode.success");
            return response()->json($result['success']);
        }

        return response()->json($result['error']);
    }


    /**
     * send verification code when reseting password
     * @param $request
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function resetPasswordGetCode($request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|min:9|max:9'
        ]);

        if($validator->fails())
            return falseValidation($validator);

        $user = User::where('phone', '=', $request->get('phone'))
            ->first();

        // if number is not exist
        $result['error']['errorMessage'] = 'Номер не существует';
        $result['error']['statusCode'] = config('constants.statusCode.incorrectConfirmationCode');

        if(!is_null($user))
        {
            $verificationCode = $this->generateVerificationCode();

            $user->verification_code = $verificationCode;
            $user->save();

            $smsMessage = 'Код - '.$verificationCode;
            $sms = new SmsController();
            $sms->sendSms($user->phone, $smsMessage, 2);

            $result['success']['verification_code'] = $user->verification_code;
            $result['success']['statusCode'] = config("constants.statusCode.success");

            return response()->json($result['success']);
        }

        return response()->json($result['error']);
    }

    /**
     * Check if driver is identified
     * @param $request
     * @return mixed
     */
    public function isDriverIdentified($request)
    {
        return User::where([
            ['id', $request->user()->id],
            ['identified', 1]
        ])->first();
    }

    /**
     * generate verification code
     * @return int
     */
    private function generateVerificationCode()
    {
        return rand(100000, 999999);
    }

    /**
     * Check should driver identify itself to get rides
     * @return boolœ
     */
    public function shouldDriverIdentified()        // 1 - driver should be identified, 2 - not identified yet (has some days left to identifying), 0 - already identified
    {
        $user = Auth::user();

        if($user->identified == 1)
            return 0;
        elseif($user->identified == 0 && $this->daysDriverToIdentification() <= 90)
            return 2;
        elseif($user->identified == 0 && $this->daysDriverToIdentification() > 90)
            return 1;

        return 1;
    }


    /**
     * Get list of users
     * @param $request
     * @return
     */
    public function usersList($request)
    {
        return User::select('id', 'phone', 'name', 'image', 'type', 'identified')->whereNotNull('type')->paginate(20);
    }


    private function daysDriverToIdentification()
    {
        $user = Auth::user();
        $userPhone = $user->phone_verified_at;

        if(!is_null($userPhone))
        {
            $phoneVerifiedDate = Carbon::parse($userPhone);
            $now = Carbon::now();

            return $phoneVerifiedDate->diffInDays($now);
        }

        return 91;
    }

}
