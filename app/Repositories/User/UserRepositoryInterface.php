<?php
/**
 * Created by PhpStorm.
 * User: helloworld
 * Date: 9/4/19
 * Time: 11:22
 */

namespace App\Repositories\User;

interface UserRepositoryInterface
{

    /**
     * Get user detail
     * @param $request
     * @return mixed
     */
    public function userDetail($request);


    /**
     * Get list of driver for identification
     * @return mixed
     */
    public function getUsersForIdentification();

    /**
     * Confirm driver identification
     * @param $request
     * @return mixed
     */
    public function identifyDriver($request);

    /**
     * login user
     * @param $request
     * @return mixed
     */
    public function login($request);

    /**
     * logout user
     * @return mixed
     */
    public function logout();

    /**
     * switch driver passenger
     * @param $request
     * @return mixed
     */
    public function switchDriverPassenger($request);

    /**
     * update user profile
     * @param $request
     * @return mixed
     */
    public function updateProfile($request);

    /**
     * driver identification
     * @param $request
     * @return mixed
     */
    public function driverUploadPhotos($request);

    /**
     * register user
     * @param $request
     * @return mixed
     */
    public function register($request);

    /**
     * verify user phone
     * @param $request
     * @return mixed
     */
    public function verifyPhone($request);

    /**
     * reset password
     * @param $request
     * @return mixed
     */
    public function resetPassword($request);

    /**
     * get verification code for reset password
     * @param $request
     * @return mixed
     */
    public function resetPasswordGetCode($request);

    public function usersList($request);

}
