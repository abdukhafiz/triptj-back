<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ride extends Model
{

    protected $table = 'rides';

    protected $fillable = [
        'from_place_id',
        'to_place_id'
    ];

    public function driver()
    {
        return $this->belongsTo('App\User', 'driver_id', 'id');
    }

    public function passenger()
    {
        return $this->belongsTo('App\User', 'passenger_id', 'id');
    }

    public function destinationFrom()
    {
        return $this->belongsTo('App\Place', 'from_place_id', 'id');
    }

    public function destinationTo()
    {
        return $this->belongsTo('App\Place', 'to_place_id', 'id');
    }

    public function prices()
    {
        return $this->hasMany('App\RidePrice', 'ride_id', 'id');
    }
}
