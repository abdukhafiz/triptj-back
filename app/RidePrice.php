<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RidePrice extends Model
{

    protected $table = 'ride_price';

    public function Ride()
    {
        return $this->belongsTo('App\Ride', 'ride_id', 'id');
    }

}
