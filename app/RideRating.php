<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RideRating extends Model
{

    protected $table = 'ride_rating';

    protected $fillable = [
        'ride_id',
        'passenger_id',
        'driver_id',
        'rating',
        'feedback'
    ];

    public function ride()
    {
        return $this->belongsTo('App\Ride', 'ride_id', 'id');
    }

    public function driver()
    {
        return $this->belongsTo('App\User', 'driver_id', 'id');
    }

    public function passenger()
    {
        return $this->belongsTo('App\User', 'passenger_id', 'id');
    }

}
