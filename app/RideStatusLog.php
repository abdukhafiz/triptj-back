<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RideStatusLog extends Model
{

    protected $table = 'ride_status_log';

    protected $fillable = [
        'ride_id',
        'user_id',
        'status'
    ];

    public function ride()
    {
        return $this->belongsTo('App\Ride', 'ride_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

}
