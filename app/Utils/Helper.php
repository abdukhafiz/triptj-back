<?php
/**
 * Created by PhpStorm.
 * User: helloworld
 * Date: 8/17/19
 * Time: 23:00
 */

namespace App\Utils;

use App\RideStatusLog;
use Illuminate\Support\Facades\Log;

/**
 * Send firebase push notification
 * @param $fcmFields
 * @return \Illuminate\Http\JsonResponse
 */
function curlFirebasePushNotification($fcmFields)
{
    $headers = array(
        'Authorization: key=' . config('constants.FCM_SERVER_KEY'),
        'Content-Type: application/json'
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, config('constants.FCM_SERVER_ADDRESS'));
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmFields));
    $curlResult = curl_exec($ch);

    if($curlResult === FALSE){
        $result['error']['statusCode'] = config("constants.statusCode.otherError");
        $result['error']['errorMessage'] = "Невозможно отправить уведомление. Пожалуйста повторите еще раз.";

        return response()->json($result['error']);
    }

    // Close connection
    curl_close($ch);
}

function customHash($str)
{
    $salt = config('constants.salt');
    return hash('sha256', $str.$salt);
}

/**
 * Validation false method
 * @param $validator
 * @return \Illuminate\Http\JsonResponse
 */
function falseValidation($validator)
{
    $errors = "";
    foreach ($validator->messages()->all() as $error) {
        $errors .= $error."\n";
    }

    $result['error']['statusCode'] = config('constants.statusCode.validationFails');
    $result['error']['errorMessage'] = $errors;

    return response()->json($result['error']);
}

/**
 * Save base64 string
 * @param $base64String
 * @param $outputFile
 * @return mixed
 */
function base64ToImage($base64String, $outputFile)
{
    $base64String = str_replace(' ', '+', $base64String);
    $decode = base64_decode($base64String);
    file_put_contents($outputFile, $decode);
}

/**
 * logging ride status
 * @param $rideId
 * @param $userId
 * @param $status
 */
function rideStatusLog($rideId, $userId, $status)
{
    // log ride status
    $logRideStatus = new RideStatusLog();
    $logRideStatus->ride_id = $rideId;
    $logRideStatus->user_id = $userId;
    $logRideStatus->status = $status;
    $logRideStatus->save();
}
