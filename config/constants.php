<?php
/**
 * Created by PhpStorm.
 * User: helloworld
 * Date: 8/18/19
 * Time: 11:00
 */

return [
    'statusCode' => [
        'success' => 1,
        'validationFails' => 2,
        'wrongHash' => 3,
        'incorrectConfirmationCode' => 4,
        'unauthorized' => 6,
        'otherError' => 5,
        'loggedOut' => 7,
        'notFound' => 8,
        'notIdentified' => 9,
        'smsNotSent' => 10
    ],
    'salt' => 'К@лӯШcha',
    'FCM_SERVER_ADDRESS' => 'https://fcm.googleapis.com/fcm/send',
    'FCM_SERVER_KEY' => 'AAAAyXRSqF0:APA91bHpT_rBs5VcrSWp0sBNp4kGgMyI7s9dyLsR_52nVZZle3WabPuj_04pSkgESDB-cNkR7jMVNG3KzePOYJtB53r4RvkO9di_FK3ZOJHabe5T9xQNN0G5h40JaFZr3bhh-Af_ljSL',
];
