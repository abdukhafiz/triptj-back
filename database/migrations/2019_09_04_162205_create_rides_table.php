<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rides', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('driver_id')
                ->nullable(true);
            $table->integer('passenger_id')
                ->nullable(true);
            $table->integer('from_place_id')
                ->nullable(false);
            $table->integer('to_place_id')
                ->nullable(false);
            $table->dateTime('ride_date_time')
                ->nullable(true);
            $table->double('price')
                ->nullable(false);
            $table->tinyInteger('status')
                ->nullable(true)
                ->comment('0 - pending (searching driver), 1 - success (rides finished), 2 - cancelled, 3 - confirmed, 4 - ride started, 5 - waiting for price confirmation');
            $table->tinyInteger('type')
                ->nullable(true);
            $table->text('comment')
                ->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rides');
    }
}
