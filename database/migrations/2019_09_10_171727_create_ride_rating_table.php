<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRideRatingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_ride_rating', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('ride_id');
            $table->integer('passenger_id');
            $table->integer('driver_id');
            $table->decimal('rating', 5, 2)
                ->nullable(true);
            $table->string('feedback')
                ->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_ride_rating');
    }
}
