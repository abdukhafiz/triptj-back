<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerUpdateDriverRate2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE TRIGGER update_driver_rate_on_update AFTER UPDATE ON
          ride_rating FOR EACH ROW
          BEGIN
            IF NEW.rating > 0 THEN
              UPDATE
                  users u
                  CROSS JOIN(
                              SELECT ROUND(AVG(rating),
                                           2) rating
                              FROM
                                ride_rating
                              WHERE
                                driver_id = NEW.driver_id
                            ) r
              SET
                u.rating = r.rating
              WHERE
                u.id = NEW.driver_id;
            END IF;
          END;
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `update_driver_rate_on_update`');
    }
}
