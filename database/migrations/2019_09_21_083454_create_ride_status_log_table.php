<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRideStatusLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ride_status_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('ride_id');
            $table->integer('user_id');
            $table->tinyInteger('status')
                ->comment('0 - pending (searching driver), 1 - success (rides finished), 2 - cancelled, 3 - confirmed, 4 - ride started');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ride_status_log');
    }
}
