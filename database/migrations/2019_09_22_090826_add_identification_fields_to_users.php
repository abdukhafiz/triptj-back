<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdentificationFieldsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->tinyInteger('identified')
                ->default(0)
                ->after('type')
                ->comment('1 - identified, 0 - not identified, 2 - waiting for request');

            $table->string('photo_passport', '255')
                ->nullable(true)
                ->after('phone_verified_at');

            $table->string('photo_driver_license', '255')
                ->nullable(true)
                ->after('photo_passport');

            $table->string('photo_car', '255')
                ->nullable(true)
                ->after('photo_driver_license');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('identified');
            $table->dropColumn('photo_passport');
            $table->dropColumn('photo_driver_license');
            $table->dropColumn('photo_car');
        });
    }
}
