<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerInsertRidePrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE TRIGGER `insert_ride_price` 
            AFTER INSERT ON `rides`
            FOR EACH ROW INSERT 
            INTO ride_price (ride_id, price, created_at) 
            VALUES (NEW.id, NEW.price, CURRENT_TIMESTAMP);
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `insert_ride_price`');
    }
}
