<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('phone', 13);
            $table->string('message');
            $table->tinyInteger('sms_type')
                ->comment('0 - sign up; 1 - driver identification; 2 - reset password;');
            $table->string('msg_id')
                ->nullable(true);
            $table->tinyInteger('retry')
                ->nullable(true);
            $table->text('server_response')
                ->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms');
    }
}
