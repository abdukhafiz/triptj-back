<?php

use App\Place;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class PlacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('places')->delete();
        $json = File::get('database/data/cities.json');
        $data = json_decode($json);
        foreach ($data as $index => $obj)
        {
            Place::create([
                'name' => $obj->name,
                'sort' => $index
            ]);
        }
    }
}
