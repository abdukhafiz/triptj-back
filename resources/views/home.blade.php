@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Подтверждение водителей</div>

                <div class="card-body">
                    @include('layouts.flash-message')

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Имя</th>
                            <th scope="col">Номер телефона</th>
                            <th scope="col">Тип</th>
                            <th scope="col">Дата рождения</th>
                            <th scope="col">Фото паспорта</th>
                            <th scope="col">Фото водительск. удостов</th>
                            <th scope="col">Фото машины</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $index => $user)
                            <tr>
                                <th scope="row">{{ $index+1 }}</th>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->phone }}</td>
                                <td>@if($user->cargo == 1) Грузовой @else Легк. машина @endif</td>
                                <td>{{ \Carbon\Carbon::parse($user->birth_date)->format('d.m.Y') }}</td>
                                <td>
                                    <img class="my-img"
                                        data-src="{{Route('resize-image', [$user->photo_passport, 4, 'height' => 500, 'width' => 700])}}"
                                        src="{{Route('resize-image', [$user->photo_passport, 4, 'height' => 500, 'width' => 700])}}"
                                        width="180" />
                                </td>
                                <td>
                                    <img class="my-img"
                                         data-src="{{Route('resize-image', [$user->photo_driver_license, 3, 'height' => 500, 'width' => 700])}}"
                                         src="{{Route('resize-image', [$user->photo_driver_license, 3, 'height' => 500, 'width' => 700])}}"
                                         width="180" />
                                </td>
                                <td>
                                    <img class="my-img"
                                         data-src="{{Route('resize-image', [$user->photo_car, 2, 'height' => 500, 'width' => 700])}}"
                                         src="{{Route('resize-image', [$user->photo_car, 2, 'height' => 500, 'width' => 700])}}"
                                         width="180" />
                                </td>
                                <td>
                                    <a href="#" class="identificate" data-id="{{ $user->id }}" data-content="1">✅</a><br />
                                    <a href="#" class="identificate" data-id="{{ $user->id }}" data-content="0">❌</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img id="img-modal-src" class="img-fluid" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<form method="post" id="identifyDriver" name="identifyDriver" class="form-horizontal d-none">
    {{ csrf_field() }}
    <input type="hidden" name="user_id" id="userIdModal" />
    <input type="hidden" name="identified" id="identifiedModal" />
    <button type="submit" class="btn btn-default"></button>
</form>

<script>
    $(document).ready(function(){
        $('.my-img').on('click', function () {
            $('#img-modal-src').attr('src', $(this).attr('data-src'));
            $('#myModal').modal('show');
        });

        $('.identificate').on('click', function () {
            var statusIdentification = $(this).attr('data-content');
            var message = '';
            if(statusIdentification == 1)
                message = 'Вы действительно хотите идентифицировать водителя?';
            else
                message = 'Вы действительно хотите не идентифицировать водителя?';

            if(confirm(message)){
                $("#userIdModal").val($(this).attr('data-id'));
                $("#identifiedModal").val(statusIdentification);

                var url = '{{ route("identify-driver") }}';

                $("#identifyDriver").attr("action", url);
                $("#identifyDriver").submit();
            }
        });
    });
</script>
@endsection
