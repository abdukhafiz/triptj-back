@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Список пользователей</div>

                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Имя</th>
                            <th scope="col">Номер телефона</th>
                            <th scope="col">Тип</th>
                            <th scope="col">Идентифицирован</th>
                            <th scope="col">Фото профиля</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $index = $users->firstItem() @endphp
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $index }}</th>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->phone }}</td>
                                <td>@if($user->cargo == 1) Грузовой @else Легк. машина @endif</td>
                                <td>@if($user->identified == 0) Не идентифицирован @elseif($user->identified == 1) Идентифицирован @elseif($user->identified == 2) Подтверждение идентификации @endif</td>
                                <td>
                                    @if(!is_null($user->image))
                                        <img class="my-img"
                                             src="{{Route('resize-image', [$user->image, 1, 'height' => 50, 'width' => 50])}}"
                                             width="50" />
                                    @else
                                        <span>Не задан</span>
                                    @endif
                                </td>
                            </tr>

                            @php $index++; @endphp
                        @endforeach
                        </tbody>
                    </table>

                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
