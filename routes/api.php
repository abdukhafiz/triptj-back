<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {

    Route::middleware('checkHash')->prefix('user')->group(function () {
        Route::post('register', 'UserController@register');
        Route::post('login', 'UserController@login');
        Route::post('verify-phone', 'UserController@verifyPhone');
        Route::post('reset-password/get-code', 'UserController@resetPasswordGetCode');
        Route::post('reset-password/reset', 'UserController@resetPassword');
    });

    Route::middleware('auth:api')->group(function () {

        Route::prefix('ride')->group(function () {
            Route::post('offer', 'RideController@offerRide');
            Route::get('get-rides', 'RideController@getRides');
            Route::get('get/{rideId}', 'RideController@get');
            Route::post('change-status', 'RideController@changeStatus');
            Route::post('accept-ride', 'RideController@acceptRide');
            Route::post('rate', 'RideController@rateRide');
            Route::post('accept-new-price', 'RideController@acceptNewPrice');
        });

        Route::prefix('user')->group(function () {
            Route::post('detail', 'UserController@detail');
            Route::post('switch-driver-passenger', 'UserController@switchDriverPassenger');
            Route::post('store-firebase-token', 'UserController@storeFirebaseToken');
            Route::get('get-firebase-token', 'UserController@getFirebaseToken');
            Route::post('update-profile', 'UserController@updateProfile');
            Route::post('upload-photos', 'UserController@driverUploadPhotos');
            Route::post('logout', 'UserController@logout');
        });

        Route::prefix('dialog')->group(function () {
            Route::get('get', 'DialogController@getDialog');
            Route::post('store-message', 'DialogController@storeMessage');
        });

    });

    Route::get('places', 'PlaceController@index');
    Route::get('img/{imageName}/{imageType}', 'ImageController@resize')->name('resize-image');       // imageType: 1 - avatar

});
