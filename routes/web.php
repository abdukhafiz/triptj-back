<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin', function () {
    return redirect('login');
});

Route::middleware('auth')->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::post('identify-driver', 'UserController@identifyDriver')->name('identify-driver');

    Route::get('users-list', 'UserController@usersList')->name('users-list');
});

//Route::get('/test/{phone}/{message}', 'SmsController@testSendSms');

Auth::routes();

Route::get('privacy', 'HomeController@privacy');
